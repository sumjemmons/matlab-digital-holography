% Model Plane Wave 
% Plane Wave equation given by 
close all; 

f = 1e14; 
omega = 2*pi*f; 
lambda = 1e-6; 
E0 = 1;
k = 2*pi/lambda; 
% k=1;

x = -10000:100:10000; 
y = -10000:100:10000;  

[X,Y]=meshgrid(x,y);
Z=(X.^2+Y.^2).^1/2;
t = linspace(0, 1/(2*pi),length(x)); 

E = E0*cos(omega*t - k.*X);
E_z = E0*cos(omega*t - k.*Z);

figure
planewave_fft = fftshift(fft2(E_z));
subplot(2,1,1); 
plane_wavefront_fft = surf(abs(planewave_fft))
% set(plane_wavefront_fft,'LineStyle','none')
title('FFT of Plane Wave - Z Direction'); 
% shading interp;


dx = 0.001;
radius_initial = 2^8*dx; 

% Going through thin lens 
% thinlens_phase = cos((k/(2*f))*(X.^2 + Y.^2)); %Use cos for real portion
% Zernike function aberrations 
energy_of_aberation = zernFun([0,0,0,0,1,0,0,0,0],X,Y,radius_initial); 

% E_postlens = E_z.*thinlens_phase; 
E_postlens = E_z.*energy_of_aberation; 
postlens_fft = fftshift(fft2(E_postlens)); 
subplot(2,1,2); 
aberrationwavefront_fft = surf(abs(postlens_fft))
% set(aberrationwavefront_fft,'LineStyle','none')
title('FFT of Plane Wave - Post Lens') 
% shading interp; 

figure
subplot(2,2,1)
surf(X,Y,E_z);
title('Plane Wave Before Lens')
shading interp;

subplot(2,2,2)
imagesc(x,y,E_z);
title('Plane Wave Before Lens')
shading interp;

subplot(2,2,3)
surf(X,Y,E_postlens);
title('Plane Wave After Lens')
shading interp;

subplot(2,2,4)
imagesc(x,y,E_postlens);
title('Plane Wave After Lens')
 

%% Reversing aberration
% energy_of_aberation_undo = zernFun([0,0,0,0,1,0,0,0,0],-X,-Y,radius_initial); 


%E_postlens_reverseab = E_postlens.*energy_of_aberation_undo; 

%negative of phase 
% E_z_neg = -E_z; 
% E_postlens_reverseab = E_postlens.*E_z_neg; 
E_postlens_reverseab_fft = (planewave_fft).*(-postlens_fft); 
% inversepostlens = ifft(postlens_fft); 
% E_postlens_reverseab = E_z.*abs((-inversepostlens)); 
E_postlens_reverseab = (ifft(E_postlens_reverseab_fft)); 

% summer:
N=100;
psf = abs(fftshift(fft2(ifftshift(postlens_fft)))).^2;
psf = psf/sum(psf(:));%Normalize to unity energy
x_psf = (-fix(N/2):fix((N-1)/2)) * dx;
figure;imagesc(x_psf*1e6,x_psf*1e6,psf);
xlabel(sprintf('Position (\\mum)'));
ylabel(sprintf('Position (\\mum)'));





figure
subplot(2,2,1)
surf(abs(E_postlens_reverseab_fft));
title('Plane Wave Corrected Aberration')
shading interp;

subplot(2,2,2)
surf(X,Y,(abs(E_postlens_reverseab)));
title('Inverse fft - time domain');
shading interp;

figure
imagesc(abs(E_postlens_reverseab))





%% 
subplot(2,2,2)
imagesc(E_postlens_reverseab_fft);
title('Plane Wave Corrected Aberration')
shading interp;




% %% 
% % New summer stuff
% superimposed_E = E + E_postlens;
% 
% figure
% subplot(2,1,1)
% surf(X,Y,superimposed_E);
% title('Plane Wave After Lens')
% shading interp;
% 
% subplot(2,1,2)
% imagesc(x,y,superimposed_E);
% title('Plane Wave After Lens')

