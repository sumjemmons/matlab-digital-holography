
xx = -50*pi:0.1*pi: 50*pi;
[XX,YY] = meshgrid(xx, xx);      %show energy density
energy = cos(XX*sin(deg2rad(5))).^2;    % 5 deg is the alpha (angle between the 2 plane waves)
contourf(XX,YY,energy);   %  --plot to make a heatmap




