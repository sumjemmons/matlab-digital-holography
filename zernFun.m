function result = calculateZernike(Zn, x, y, Rn)
% function [result,polystr] = calculateZernike(p, rho, theta)
%
%  Calculates the Zernike polynomial value for the given pixel location.
%
%   INPUT:
%     Zn          = fringe polynomial coefficients. 
%     x,y         = spatial coordinate in mm
%     Rn          = Normalization radius
%
%   OUTPUT
%     results = The computed wavefront value (in waves).

%   The following table lists the first 15 Zernike functions.

%
%       n    m    Zernike function             Normalization
%       ----------------------------------------------------
%       0    0    1                              1/sqrt(pi)
%       1    1    r * cos(theta)                 2/sqrt(pi)
%       1   -1    r * sin(theta)                 2/sqrt(pi)
%       2    2    r^2 * cos(2*theta)             sqrt(6/pi)
%       2    0    (2*r^2 - 1)                    sqrt(3/pi)
%       2   -2    r^2 * sin(2*theta)             sqrt(6/pi)
%       3    3    r^3 * cos(3*theta)             sqrt(8/pi)
%       3    1    (3*r^3 - 2*r) * cos(theta)     sqrt(8/pi)
%       3   -1    (3*r^3 - 2*r) * sin(theta)     sqrt(8/pi)
%       3   -3    r^3 * sin(3*theta)             sqrt(8/pi)
%       4    4    r^4 * cos(4*theta)             sqrt(10/pi)
%       4    2    (4*r^4 - 3*r^2) * cos(2*theta) sqrt(10/pi)
%       4    0    6*r^4 - 6*r^2 + 1              sqrt(5/pi)
%       4   -2    (4*r^4 - 3*r^2) * sin(2*theta) sqrt(10/pi)
%       4   -4    r^4 * sin(4*theta)             sqrt(10/pi)
%       ----------------------------------------------------

rho = (x.^2 + y.^2).^0.5 ;
theta = atan2(y,x);

nv = [0 1 1 2 2 2 3 3 3 3 4 4 4 4 4 3 3 4 4 5 5 6 4 4 5 5 6 6 7 7 8 5 5 6 6 7 7 8 8 9 9 10 12];
mv = [0 1 1 2 0 2 3 1 1 3 4 2 0 2 4 3 3 2 2 1 1 0 4 4 3 3 2 2 1 1 0 5 5 4 4 3 3 2 2 1 1 0 0];
scv = ' cs cscsccs scs cscscscs cscscscscs  ';

% nv = [0 1 1 2 2 2 3 3 3 3 4 4 4 4 4 5 5  5 5  5 5 6  6 6  6 6  6 6  7 7  7 7  7 7  7 7 8  8 8  8 8  8 8  8 8  9 9  9 9  9 9  9 9  9 9 10 10 10 10 10 10 10 10 10  10 10 11 11 11 11 11 11 11 11 11 11  11 11 12 12 12 12 12 12 12 12 12  12 12  12 12 13 13 13 13 13 13 13 13 13 13  13 13  13 13];
% mv = [0 -1 1 0 -2 2 -1 1 -3 3 0 -2 2 -4 4 -1 1 -3 3 -5 5 0 -2 2 -4 4 -6 6 -1 1 -3 3 -5 5 -7 7 0 -2 2 -4 4 -6 6 -8 8 -1 1 -3 3 -5 5 -7 7 -9 9  0 -2  2 -4  4 -6  6 -8  8 -10 10 -1  1 -3  3 -5  5 -7  7 -9  9 -11 11  0 -2  2 -4  4 -6  6 -8  8 -10 10 -12 12 -1  1 -3  3 -5  5 -7  7 -9  9 -11 11 -13 13];
% scv = ' csc sccsscc ss cscscscs cscscscscs  ';

result = 1*rho;
for iZern = 1:length(Zn)  % loop to the last aberation (spherical is 9)
    n = nv(iZern);
    m = mv(iZern);
    sc = scv(iZern);
    
%     polystr = '(';
    % calculate radial part Rnm
    Rnm = zeros(size(rho));
    for s=0:(n-m)/2
        numerator = (-1)^s * factorial(n-s);
        denominator = factorial(s)*factorial((n+m)/2-s)*factorial((n-m)/2-s);
        Rnm = Rnm + (numerator / denominator) * rho.^(n-2*s);
%         if s>0
%             polystr = sprintf('%s + ',polystr);
%         end
%         polystr = sprintf('%s%dr^%d',polystr,(numerator / denominator),(n-2*s));
    end % for s statement
%     polystr = sprintf('%s) ',polystr);
    
    % 3 cases.  sc=' ', 's', 'c'.
    switch sc
        case ' '
            % means m=0
%             if n = 2
%                 result = result + Zn(iZern)*Rnm - 1;
%             elseif n = 4
%                 result = result + Zn(iZern)*Rnm + 1;
        result = result + Zn(iZern)*Rnm;

        case 's'
            result = result + Zn(iZern)*Rnm .* sin(m*theta);
%             polystr = sprintf('%s sin(%dA)',polystr,m);
        case 'c'
            result = result + Zn(iZern)*Rnm .* cos(m*theta);
%             polystr = sprintf('%s cos(%dA)',polystr,m);
    end % switch sc
end
end