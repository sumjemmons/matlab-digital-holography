% Model Plane Wave 
% Plane Wave equation given by 
close all; 

f = 1e14; 
omega = 2*pi*f; 
lambda = 1e-6; 
E0 = 1;
k = 2*pi/lambda; 
% k=1;

x = -10000:100:10000; 
y = -10000:100:10000;  

[X,Y]=meshgrid(x,y);
Z=(X.^2+Y.^2).^1/2;
t = linspace(0, 1/(2*pi),length(x)); 

E = E0*cos(omega*t - k.*X);
E_z = E0*cos(omega*t - k.*Z);


% figure
% subplot(3,1,1)
% surf(X,Y,E);
% shading interp;
% view(0,120)
% title('Plane Wave Before Lens')
% subplot(3,1,2)
% imagesc(x,y,E);
% shading interp;
% title('Plane Wave Before Lens')
% subplot(3,1,3)
% surf(X,Y,E_z);
% view(0,120)
% shading interp;
% title('Plane Wave Before Lens - Z Direction')

dx = 0.001;
radius_initial = 2^8*dx; 

% Going through thin lens 
% thinlens_phase = cos((k/(2*f))*(X.^2 + Y.^2)); %Use cos for real portion
energy_of_aberation = zernFun([0,1,0,0,0,0,0,0,0],X,Y,radius_initial); 

% E_postlens = E_z.*thinlens_phase; 
E_postlens = E_z.*energy_of_aberation; 


figure
subplot(2,2,1)
surf(X,Y,E_z);
title('Plane Wave Before Lens')
shading interp;

subplot(2,2,2)
imagesc(x,y,E_z);
title('Plane Wave Before Lens')
shading interp;

subplot(2,2,3)
surf(X,Y,E_postlens);
title('Plane Wave After Lens')
shading interp;

subplot(2,2,4)
imagesc(x,y,E_postlens);
title('Plane Wave After Lens')
 

% Reversing aberration
energy_of_aberation_undo = zernFun([0,-1,0,0,0,0,0,0,0],X,Y,radius_initial); 


E_postlens_reverseab = E_postlens.*energy_of_aberation_undo; 

figure
subplot(2,2,1)
surf(X,Y,E_postlens_reverseab);
title('Plane Wave Before Lens')
shading interp;

subplot(2,2,2)
imagesc(E_postlens_reverseab);
title('Plane Wave Before Lens')
shading interp;




%% 
% New summer stuff
superimposed_E = E + E_postlens;

figure
subplot(2,1,1)
surf(X,Y,superimposed_E);
title('Plane Wave After Lens')
shading interp;

subplot(2,1,2)
imagesc(x,y,superimposed_E);
title('Plane Wave After Lens')

