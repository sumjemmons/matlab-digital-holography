% Model Plane Wave 
% Plane Wave equation given by 
close all; 
% updated some formulas based on this site:
% https://phys.libretexts.org/Bookshelves/University_Physics/Book%3A_University_Physics_(OpenStax)/Book%3A_University_Physics_II_-_Thermodynamics_Electricity_and_Magnetism_(OpenStax)/16%3A_Electromagnetic_Waves/16.03%3A_Plane_Electromagnetic_Waves#:~:text=We%20now%20consider%20solutions%20to,kx%E2%88%92%CF%89t).&text=The%20frequency%20f%20is%20the,by%20%CF%89%3D2%CF%80f.
% Might be wrong/different than what you were doing
% Not sure if t should be 0 or not 
% Not sure about the E_z graph, I know the fringe pattern is what we are
% looking for 
% We are getting further though
 
 
lambda = 632.8e-9; 
E0 = 1;
T=lambda/(3*10^8)
freq = 1/T
k = 2*pi/lambda; 
omega = 2*pi*freq;
% k=1;



x = -10e-3:0.1e-3:10e-3; 
y = -10e-3:0.1e-3:10e-3;  

[X,Y]=meshgrid(x,y);
Z=(X.^2+Y.^2).^1/2;
t = linspace(0, 1/(2*pi),length(x)); 
% t = 0;
E1 = E0*cos(k.*X - omega*t);
E2 = E0*cos(k.*Z - omega*t);


%E = E0*cos(k.*X - omega*t - phaseX);
%E_z = E0*cos(k.*Z - omega*t - phaseZ);


figure
subplot(3,1,1)
surf(X,Y,E);
shading interp;
view(0,120)
title('Plane Wave Before Lens')
subplot(3,1,2)
imagesc(x,y,E);
shading interp;
title('Plane Wave Before Lens')
subplot(3,1,3)
surf(X,Y,E_z);
view(0,120)
shading interp;
title('Plane Wave Before Lens - Z Direction')


% Going through thin lens 
thinlens_phase = cos((k/(2*freq))*(X.^2 + Y.^2)); %Use cos for real portion

E_postlens = E_z.*thinlens_phase; 

figure
subplot(2,2,1)
surf(X,Y,E_z);
title('Plane Wave Before Lens')
shading interp;

subplot(2,2,2)
imagesc(x,y,E_z);
title('Plane Wave Before Lens')
shading interp;

subplot(2,2,3)
surf(X,Y,E_postlens);
title('Plane Wave After Lens')
shading interp;

subplot(2,2,4)
imagesc(x,y,E_postlens);
title('Plane Wave After Lens')
